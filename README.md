[![pipeline status](https://gitlab.com/swm2022/pytest/badges/master/pipeline.svg)](https://gitlab.com/swm2022/pytest/-/commits/master)
[![coverage report](https://gitlab.com/swm2022/pytest/badges/master/coverage.svg)](https://gitlab.com/swm2022/pytest/-/commits/master)

# Pytest demo

This is pytest demo repository.

This CI demostrates:
* How to create pytest and coverage HTML reports.
* How to integrate code coveradge within MR.

## Install

Prepare environment:

```console
virtualenv venv -p python3
source venv/bin/activate
```

You can install pytest like this:

```console
pip install pytest-html
```

You can enable HTML repotrs with:

```console
pip install pytest-html
```

## Usage

You can create coveraget and test reports:

```console
pytest --html=public/report.html --cov=. --cov-report html
```

```console
coverage run -m pytest
coverage report
coverage xml
```

## Integration to Gitlab MR

Integration is described at https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html

## Badges

Info on GL badges https://docs.gitlab.com/ee/ci/pipelines/settings.html#latest-release-badge

Regex for Pytest coverage percentage can be found at https://regex101.com/r/gpVjKW/1
